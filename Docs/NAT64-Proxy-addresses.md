# NAT64 Proxy addressing scheme.
<!-- pagebreak -->

## Table of contents
- [NAT64 Proxy addressing scheme.](#nat64-proxy-addressing-scheme)
    - [Table of contents](#table-of-contents)
    - [Preface](#preface)
    - [IP scheme](#ip-scheme)

<!-- pagebreak -->

## Preface
* This document will contain a table of the NDP and ARP proxy addresses used, and where they are used for static NAT.

* Table is found below.

<!-- pagebreak -->

## IP scheme
| **Device** | **Proxy adress type** | **Proxy address** | **On interface** | **Subnet** | **To server IP**      |
| ---------- | --------------------- | ----------------- | ---------------- | ---------- | --------------------- |
| DNS_SRX    | NDP-PROXY             | 2001:0:2:20::3    | ge-0/0/0.0       | /64        | 10.69.69.2 (DNS-MPLS) |
| DNS_SRX    | ARP-PROXY             | 10.69.69.5        | ge-0/0/1.0       | /24        | 10.69.69.2 (DNS-MPLS) |